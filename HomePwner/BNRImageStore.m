//
//  BNRImageStore.m
//  HomePwner
//
//  Created by John Gallagher on 1/7/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "BNRImageStore.h"


@interface BNRImageStore ()

@property (nonatomic, strong) NSMutableDictionary *dictionary;

- (NSString *)imagePathForKey:(NSString *)key;

@end

@implementation BNRImageStore

+ (instancetype)sharedStore
{
    static BNRImageStore *sharedStore;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedStore = [[self alloc]initPrivate];
    });
    
//    if (!sharedStore) {
//        sharedStore = [[self alloc] initPrivate];
//    }

    return sharedStore;
}

// No one should call init
- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[BNRImageStore sharedStore]"
                                 userInfo:nil];
    return nil;
}

// Secret designated initializer
- (instancetype)initPrivate
{
    self = [super init];

    if (self) {
        _dictionary = [[NSMutableDictionary alloc] init];
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(clearCache:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        
    }

    return self;
}

- (void)setImage:(UIImage *)image forKey:(NSString *)key
{
    self.dictionary[key] = image;
    
    NSString *path = [self imagePathForKey:key];

//ch.18 bronze challenge
    NSData *data = UIImagePNGRepresentation(image);
//    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    [data writeToFile:path atomically:YES];
}

- (UIImage *)imageForKey:(NSString *)key
{
//    return self.dictionary[key];
    UIImage *result = self.dictionary[key];
    
    if (!result) {
        NSString *path = [self imagePathForKey:key];
        result = [UIImage imageWithContentsOfFile:path];
        
        if (result) {
            self.dictionary[key] = result;
        } else {
            NSLog(@"Error. Couldn't finde image for key.");
        }
    }
    
    return result;
}

- (void)deleteImageForKey:(NSString *)key
{
    if (!key) {
        return;
    }
    [self.dictionary removeObjectForKey:key];
    
    NSString *imagePath = [self imagePathForKey:key];
    [[NSFileManager defaultManager]removeItemAtPath:imagePath error:nil];
}

#pragma ch.18 Archiving

- (NSString *)imagePathForKey:(NSString *)key
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    
    return [documentDirectory stringByAppendingPathComponent:key];
}

- (void)clearCache:(NSNotification *)note
{
    NSLog(@"Flushing %lu images out of the cache.", [self.dictionary count]);
    
    [self.dictionary removeAllObjects];
}

@end
